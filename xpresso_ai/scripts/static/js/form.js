$(document).ready(function() {

	$('form').on('submit', function(event) {
        $('#output').val('')
        var data = {
        "input":{
                "inputseq" : $('#InputSequence').val(),
                "topk" : $('#TopkNumber').val(),
                "model" : $('#modelSelection').val()
            }
         }
		$.ajax({
		    data : JSON.stringify(data),
			contentType: "application/json;charset=utf-8",
			type : 'POST',
			url : '/predict'
		})
		.done(function(data) {

			if (data.error) {
				$('#output').val(data.error)
			}
			else {
			    $('#output').val(data.results.prediction)
			}

		});

		event.preventDefault();

	});

});