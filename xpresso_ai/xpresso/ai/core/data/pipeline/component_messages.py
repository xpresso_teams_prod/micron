from enum import  Enum


class ComponentMessage(Enum):
    """
    Enum class to standardize all the database collection values(predefined)
    for certain fields
    """

    COMPONENT_STARTED_MESSAGE = "Component Started"
    COMPONENT_RESTARTED_MESSAGE = "Component Restarted"
    COMPONENT_PAUSED_MESSAGE = "Component Paused"
    COMPONENT_TERMINATED_MESSAGE = "Component Terminated"
    COMPONENT_COMPLETED_MESSAGE = "Component Completed"

    def __str__(self):
        return self.value
