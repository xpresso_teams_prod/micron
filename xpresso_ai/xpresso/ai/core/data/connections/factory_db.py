""" Factory class for database connectors in Data-Connectivity """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import enum
import xpresso.ai.core.data.connections.connector_exception as connector_exception
from xpresso.ai.core.data.connections.external import DBConnector
from xpresso.ai.core.data.connections.external import GoogleBigQueryConnector


class DBType(enum.Enum):
    """

    Enum Class that lists types of DBMS supported by DBConnector class

    """

    Presto = "Presto"
    BigQuery = "BigQuery"


class DBConnectorFactory:
    """

    Factory class to provide Connector object of specified type

    """

    @staticmethod
    def getconnector(datasource_type):
        """

        This method returns Connector object of a specific datasource

        :param datasource_type: a string object stating the
                                datasource

        :return: Connector object
        """

        if datasource_type == DBType.BigQuery.value:
            return GoogleBigQueryConnector()
        else:
            return DBConnector()
