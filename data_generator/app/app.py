"""
    Data generation module
"""

import json
import os
import pickle
from random import seed

import numpy as np
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Sanyog Vyawahare"

logging = XprLogger("data_generator")


# Folder creating function
def create_folders(dirnames, pathname):
    """ Creating folder if not present """
    for dirname in dirnames:
        folder_name = pathname + dirname
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)


# Model saving function
def save_model(data, filename):
    """ Saving graph """
    output_dict = open(filename, 'wb')
    pickle.dump(data, output_dict)
    output_dict.close()
    return


# Sequence saving function
def save_seq(filename, data):
    """ Saving sequence """
    buffer_writer = open(filename, "w")
    for line in data:
        line_temp = [str(item) for item in line]
        line_temp = " ".join(line_temp) + "\n"
        buffer_writer.writelines(line_temp)
    buffer_writer.close()


# Data generation class
class DGeneration(object):
    """ Declaring and defining class objects """

    def __init__(self):
        """ Initialising variables """

        # Reading config data from json
        self.config_path = 'config/config.json'
        self.config_file = open(self.config_path, 'r')
        self.json_object = json.load(self.config_file)
        self.config_file.close()

        self.data_prep_param = self.json_object['data_preprocess']
        self.data_model = self.json_object['model']['pg_bn']

        self.inputfile = self.data_prep_param['input_file']
        self.cutting_window = self.data_prep_param['cutting_window']
        self.read_write_flag = self.data_prep_param['use_read_write_operation_feature']
        self.splitperc = self.data_prep_param['train_split_ratio']
        self.seed_val = self.data_prep_param['seed']

        self.filename = str(self.inputfile.split('/')[-1].split('.')[0])
        self.master_path = self.data_prep_param['master_path']
        self.master_path = self.master_path.replace("FILE", self.filename)
        self.master_path = self.master_path.replace("CUTTING", str(self.cutting_window))
        self.master_path = self.master_path.replace("SPLIT", str(self.splitperc))
        self.master_path = self.master_path.replace("RW", str(self.read_write_flag))
        self.mountedpath = self.data_prep_param['mounted_path'] + self.master_path

        self.datapath = self.mountedpath + self.data_prep_param['data_path']
        self.trainfile = self.datapath + self.data_prep_param['train_file']
        self.validatefile = self.datapath + self.data_prep_param['validate_file']
        self.trainfileseq = self.datapath + self.data_prep_param['train_file_seq']
        self.validatefileseq = self.datapath + self.data_prep_param['validate_file_seq']

        self.folders = self.data_prep_param['folders']
        self.logpath = self.datapath + self.data_prep_param['logfile']

        self.snia_op = self.data_prep_param['convert_mode']['snia_op']
        self.op_val = self.data_prep_param['convert_mode']['op_val']

        self.my_logger = logging

    def get_sequence_wo_shuffle(self, lines, limit):
        """Formatting data then shuffling"""
        lines_str = [' '.join([str(it) for it in item]) for item in lines]
        seed(self.seed_val)
        # shuffle(lines_str)
        save_model(lines_str[:limit], self.trainfileseq.replace('.txt', '.pkl'))
        save_model(lines_str[limit:], self.validatefileseq.replace('.txt', '.pkl'))

    def prepare_sequence_and_shuffle(self):
        """ Reading file and dividing block access into sequences """
        previous, sentencesb, lines = 0, [], []
        with open(self.inputfile) as file_obj:
            for line_num, line in enumerate(file_obj):
                total_records = int(float(line_num))
        estimated_time = np.ceil(total_records / (400000 * 60.0))
        output_string = "Estimated Time " + str(estimated_time) + "mins"
        print(output_string)
        self.my_logger.info(output_string)
        with open(self.inputfile) as file_obj:
            for line_num, line in enumerate(file_obj):
                if not (line_num + 1) % 1000000:
                    loading_val = round(float(line_num) * 100 / float(total_records + 1), 2)
                    self.my_logger.info("loading file %s ", str(loading_val))
                    print(f"loading file {str(loading_val)} + %")
                fields = line.replace('\n', '').split(";")
                timestamp = float(fields[0])
                sec_num = int(fields[2])
                if self.read_write_flag:
                    operation = self.snia_op[fields[5].strip()]
                    disk_id = sec_num * 10 + self.op_val[operation]
                else:
                    disk_id = sec_num
                if (timestamp - previous > self.cutting_window) and previous:
                    lines.append(sentencesb)
                    sentencesb = []
                sentencesb += [disk_id]
                previous = timestamp
            if sentencesb:
                lines.append(sentencesb)

        limit = int(float(len(lines)) * self.splitperc)
        self.get_sequence_wo_shuffle(lines, limit)
        seed(self.seed_val)
        # shuffle(lines)
        save_seq(self.trainfileseq, lines[:limit])
        save_seq(self.validatefileseq, lines[limit:])

    def main(self):
        """ Main method"""

        # Creating folders
        create_folders(self.folders, self.mountedpath)

        self.my_logger.info("Process started")
        print("Process started")

        # Generating sequences and divide sequences into train and test
        self.my_logger.info("Generating sequences and divide sequences into train and test")
        print("Generating sequences and divide sequences into train and test")
        self.prepare_sequence_and_shuffle()


class DataGenerator(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="DataGenerator")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


# Creating class object
CLASS_OBJ = DGeneration()

# Calling main method of class
CLASS_OBJ.main()

# if __name__ == "__main__":
#     # To run locally. Use following command:
#     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py
#
#     data_prep = DataGenerator()
#     if len(sys.argv) >= 2:
#         data_prep.start(run_name=sys.argv[1])
#     else:
#         data_prep.start(run_name="")
